{
  localflow:: {
    toplevel: 'specs/rome',
    workflow: 'workflow.yml',
  },
  initdata: {
    dxaod_file: "https://recastwww.web.cern.ch/recastwww/data/DAOD_SUSY10.12528061._000001.pool.root.1",
    did: 404951,
    xsec_in_pb: 1.735e-3
  }
}
