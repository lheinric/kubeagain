{
  local base = self,
  // "initdata": base.initdata,
  // "workflow": base.workflow,
  // "toplevel": base.toplevel,
  // "dataarg": base.workdir,
  "ctrlopts": {
    "disable_prepub": true
  },
  "backendopts": {
    "path_base": "/data/",
    "purepubopts": {
      "exec": {
        "logging": false
      }
    },
    "kubeconfig": "incluster",
    "claim_name": "mylocal-pvc"
  },
  "backend": "kubedirectjob",
  "visualize": true,
  "plugins": [
    "yadageextresult.pktbackend"
  ],
}

