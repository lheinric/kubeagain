local templates= import 'manifests.libsonnet';
local runtime = std.extVar("run");
local argument = std.extVar("arg");



local final = if runtime.remote then
  runtime + {workflowspec:: argument} 
  else
  runtime + {workflowspec:: argument + {workflow_json: std.extVar("wflow")}}
;
    
[
  templates.cfgmap + final,
  templates.job + final
]