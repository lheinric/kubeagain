local remotespec= import 'wflowspec.libsonnet';
local localspec= import 'localspec.libsonnet';
{
cfgmap: {
  local base = self,
  local remoteorlocal = if base.remote then remotespec else localspec,
  local patch = if base.remote then 
    {
      initdata: base.workflowspec.initdata,
      workflow: base.workflowspec.workflow,
      toplevel: base.workflowspec.toplevel
    }
    else
    {
      initdata: base.workflowspec.initdata,
      workflow_json: base.workflowspec.workflow_json,
    },

  local workflowspec = {
      dataarg: "cifwflow-"+base.wflowid,
  } + patch,

  "apiVersion": "v1",
    "kind": "ConfigMap",
    "data": {
      "workflow.yml": std.toString(remoteorlocal + workflowspec)
    },
    "metadata": {
      "name": "workflow-config-"+base.wflowid
    }


},
job: {
    local base = self,
    "apiVersion": "batch/v1",
    "kind": "Job",
    "spec": {
      "backoffLimit": 0,
      "template": {
        "spec": {
          "restartPolicy": "Never",
          "containers": [
            {
              "image": "lukasheinrich/yadkube",
              "command": [
                "yadage-run",
                "-f",
                "/etc/config/workflow.yml"
              ],
              "volumeMounts": [
                {
                  "name": "data",
                  "mountPath": "/data"
                },
                {
                  "name": "config-volume",
                  "mountPath": "/etc/config"
                }
              ],
              "workingDir": "/data",
              "name": "runner"
            }
          ],
          "volumes": [
            {
              "persistentVolumeClaim": {
                "claimName": "mylocal-pvc"
              },
              "name": "data"
            },
            {
              "configMap": {
                "name": "workflow-config-"+base.wflowid
              },
              "name": "config-volume"
            }
          ]
        }
      }
    },
    "metadata": {
      "name": "workflow-"+base.wflowid
    }
  }
}
